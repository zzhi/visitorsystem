﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;
namespace VMS.Manager
{
    public static class PictureToByte
    {

        public static BitmapImage ByteArrayToBitmapImage(byte[] byteArray)
        {
            BitmapImage bmp;

            try
            {
                bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = new MemoryStream(byteArray);
                bmp.EndInit();
            }
            catch
            {
                bmp = null;
            }

            return bmp;
        }


        public static byte[] BitmapImageToByteArray(BitmapImage bmp)
        {
            var byteArray = new byte[] {};

            try
            {
                Stream sMarket = bmp.StreamSource;

                if (sMarket != null && sMarket.Length > 0)
                {
                    sMarket.Position = 0;

                    using (var br = new BinaryReader(sMarket))
                    {
                        byteArray = br.ReadBytes((int) sMarket.Length);
                    }
                }
            }
            catch (Exception)
            {
                byteArray = null;
            }

            return byteArray;
        }
    }
}
