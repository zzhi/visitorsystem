﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace VMS.Manager
{
    class ByteArrayToBitmapImageConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            
            if (value == null)
            {
                return null;  
            }
            var byteArray = value as byte[];
            return PictureToByte.ByteArrayToBitmapImage(byteArray);


        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            var bmp = value as BitmapImage;
            if (bmp == null)
                return null;
            return PictureToByte.BitmapImageToByteArray(bmp);

        }
    }
}
