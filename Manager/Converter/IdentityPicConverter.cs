﻿using System;
using System.Windows.Data;

namespace VMS.Manager
{
    internal class IdentityPicConverter:IValueConverter
    {

        public object Convert(object value, Type targetType, object parameters, System.Globalization.CultureInfo culture)
        {
            string path;
            if (value != null)
            {
                string name = value.ToString();

                path= @AppDomain.CurrentDomain.BaseDirectory+"IdentityPics\\" + name + ".jpg";
            }
            else
            {
                path= @AppDomain.CurrentDomain.BaseDirectory+"IdentityPics\\default.jpg";
            }


            return path;


        }

        public object ConvertBack(object value, Type targetType, object parameters,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
