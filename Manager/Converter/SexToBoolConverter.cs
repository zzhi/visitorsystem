﻿using System;
using System.Windows.Data;
using VMS.Common;

namespace VMS.Manager
{
    public class SexToBoolConverter : IValueConverter
    {
       
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Sex s = (Sex) value;
            return s == (Sex) int.Parse(parameter.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            bool isChecked = (bool) value;
            if (!isChecked)
            {
                return null;
            }
            return (Sex) int.Parse(parameter.ToString());
        }
    }



    public class BooltoTextConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool b = value is bool ? (bool) value : false;

            return b ? "离开" : "未离开";

        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return false;
        }
    }
}
