﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Manager
{
    [Serializable]
   public  class VisitInfoListViewMolde:ViewModelBase
    {

        ObservableCollection<VisitInfoViewMolde> _visitInfoItems = new ObservableCollection<VisitInfoViewMolde> { };

        public ObservableCollection<VisitInfoViewMolde> VisitInfoItems
        {
            get { return _visitInfoItems; }
            set
            {
                _visitInfoItems = value;
                RaisePropertyChanged(() => VisitInfoItems);
            }
        }


        public void AddItem(VisitInfoViewMolde s)
        {
            _visitInfoItems.Add(s);
        }

        public void ClearAll()
        {
            _visitInfoItems.Clear();
        }
    }
}
