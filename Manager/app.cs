﻿using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Configuration;
using System.Windows;

namespace VMS.Manager
{
    public class EntryPoint
    {
        [STAThread]
        public static void Main(string[] args)
        {
            var manager = new SingleInstanceManager();
            manager.Run(args);
        }
    }

    public class SingleInstanceApplication : Application
    {
        public static String Directory;

        public SingleInstanceApplication()
        {
        }

        public void Activate()
        {
            // Reactivate application's main window
            this.MainWindow.Activate();
            
        }

        protected override void OnStartup(System.Windows.StartupEventArgs e)
        {
            string lt = ConfigurationManager.AppSettings.Get("lang");
            if (lt == "0")//0:zh-CN;1:en-US
            {
                Langs.SetLanguage(LangType.ZhCn);
            }
            else
            {
                Langs.SetLanguage(LangType.EnUs);
            }

            base.OnStartup(e);
            // Create and show the application's main window
            var window = new MainWindow();
            
            window.Show();

            //WindowTest t = new WindowTest();
            //t.Show();
        }
    }

    // Using VB bits to detect single instances and process accordingly:
    //  * OnStartup is fired when the first instance loads
    //  * OnStartupNextInstance is fired when the application is re-run again
    //    NOTE: it is redirected to this instance thanks to IsSingleInstance
    public class SingleInstanceManager : WindowsFormsApplicationBase
    {
        private SingleInstanceApplication app;

        public SingleInstanceManager()
        {
            this.IsSingleInstance = true;
        }

        protected override bool OnStartup(Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e)
        {
            // First time app is launched
            app = new SingleInstanceApplication();
            app.Run();
            return false;
        }

        protected override void OnStartupNextInstance(StartupNextInstanceEventArgs eventArgs)
        {
            // Subsequent launches

            base.OnStartupNextInstance(eventArgs);
            app.Activate();
        }
    }
}