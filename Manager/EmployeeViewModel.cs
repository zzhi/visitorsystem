﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Model;
namespace VMS.Manager
{
    [Serializable]
    public class EmployeeViewModel : ViewModelBase
    {
        private string _empname;
        private string _company;
        private string _position;
        private string _email;
        private string _tel;
        private string _floor;
        private string _descriptions;

        public EmployeeViewModel()
        {
        }


        public EmployeeViewModel(Employee emp)
        {
             _company = emp.Company;
             _position = emp.Position;
             _email = emp.Email;
             _tel = emp.Tel;
             _floor = emp.Floor;
             _descriptions = emp.Descriptions;
             _empname = emp.EmpName;
        }


        public string EmpName
        {
            get { return _empname; }
            set
            {
                _empname = value;
                RaisePropertyChanged(() => EmpName);
            }
        }

        public string Company
        {
            get { return _company; }
            set
            {
                _company = value;
                RaisePropertyChanged(() => Company);
            }
        }


        public string Position
        {
            get { return _position; }
            set
            {
                _position = value;
                RaisePropertyChanged(() => Position);
            }
        }

        public string Floor
        {
            get { return _floor; }
            set
            {
                _floor = value;
                RaisePropertyChanged(() => Floor);
            }
        }

        public string Tel
        {
            get { return _tel; }
            set
            {
                _tel = value;
                RaisePropertyChanged(() => Tel);
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged(() => Email);
            }
        }


        public string Descriptions
        {
            get { return _descriptions; }
            set
            {
                _descriptions = value;
                RaisePropertyChanged(() => Descriptions);
            }
        }
    }
}
