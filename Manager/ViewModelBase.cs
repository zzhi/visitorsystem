﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using VMS.Manager.Properties;

namespace VMS.Manager
{

     [Serializable]
    public class ViewModelBase : INotifyPropertyChanged
    {
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            if (PropertyChanged == null)
            {
                return;
            }
            if (propertyExpression == null)
            {
                throw new ArgumentNullException("propertyExpression");
            }
            var memberExpression = propertyExpression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new ArgumentException(Resources.ModelBase_RaisePropertyChanged_Invalid_argument_, "propertyExpression");
            }
            var propertyInfo = memberExpression.Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException(Resources.ModelBase_RaisePropertyChanged_Argument_is_not_a_property_, "propertyExpression");
            }
            PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }
    }
}
