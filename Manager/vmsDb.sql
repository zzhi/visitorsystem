USE [VMS]
GO
/****** Object:  Table [dbo].[VMS_Employee]    Script Date: 11/21/2014 14:11:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VMS_Employee](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[EmpName] [nvarchar](50) NOT NULL,
	[Company] [nvarchar](100) NOT NULL,
	[Position] [nvarchar](100) NULL,
	[Email] [nvarchar](200) NULL,
	[Tel] [nvarchar](20) NULL,
	[Floor] [nvarchar](20) NULL,
	[Descriptions] [nvarchar](500) NULL,
 CONSTRAINT [PK_VMS_Employee] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'被访者姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VMS_Employee', @level2type=N'COLUMN',@level2name=N'EmpName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VMS_Employee', @level2type=N'COLUMN',@level2name=N'Company'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'职位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VMS_Employee', @level2type=N'COLUMN',@level2name=N'Position'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VMS_Employee', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VMS_Employee', @level2type=N'COLUMN',@level2name=N'Tel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'楼层' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VMS_Employee', @level2type=N'COLUMN',@level2name=N'Floor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VMS_Employee', @level2type=N'COLUMN',@level2name=N'Descriptions'
GO
/****** Object:  Table [dbo].[VMS_Visitor]    Script Date: 11/21/2014 14:11:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VMS_Visitor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Identity] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[SEX] [bit] NULL,
	[Nationality] [nvarchar](20) NULL,
	[Birthday] [date] NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[IdentityPic] [image] NULL,
	[IdentityImg] [image] NULL,
	[Descriptions] [nvarchar](500) NULL,
	[Address] [nvarchar](200) NULL,
	[Company] [nvarchar](100) NULL,
	[CompanyAddress] [nvarchar](200) NULL,
	[Position] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Tel] [nvarchar](20) NULL,
	[PostCode] [nvarchar](20) NULL,
	[Fax] [nvarchar](20) NULL,
	[CurrentImg] [image] NULL,
	[Descriptions1] [nvarchar](500) NULL,
 CONSTRAINT [PK_VMS_Visitor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VMS_VisitInfo]    Script Date: 11/21/2014 14:11:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VMS_VisitInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[VisitorId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[VisitorCardInfo] [nvarchar](100) NOT NULL,
	[RegisterTime] [datetime] NOT NULL,
	[CheckinTime] [datetime] NULL,
	[Leave] [bit] NOT NULL,
	[LeaveTime] [datetime] NULL,
 CONSTRAINT [PK_VMS_VisitInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_VMS_VisitInfo_VMS_Employee]    Script Date: 11/21/2014 14:11:28 ******/
ALTER TABLE [dbo].[VMS_VisitInfo]  WITH CHECK ADD  CONSTRAINT [FK_VMS_VisitInfo_VMS_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[VMS_Employee] ([id])
GO
ALTER TABLE [dbo].[VMS_VisitInfo] CHECK CONSTRAINT [FK_VMS_VisitInfo_VMS_Employee]
GO
/****** Object:  ForeignKey [FK_VMS_VisitInfo_VMS_Visitor]    Script Date: 11/21/2014 14:11:28 ******/
ALTER TABLE [dbo].[VMS_VisitInfo]  WITH CHECK ADD  CONSTRAINT [FK_VMS_VisitInfo_VMS_Visitor] FOREIGN KEY([VisitorId])
REFERENCES [dbo].[VMS_Visitor] ([id])
GO
ALTER TABLE [dbo].[VMS_VisitInfo] CHECK CONSTRAINT [FK_VMS_VisitInfo_VMS_Visitor]
GO
