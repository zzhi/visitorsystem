﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Model;

namespace VMS.Manager
{
    [Serializable]
    public class VisitInfoViewMolde : ViewModelBase
    {
        private string _vistorName;
        private string _empName;
        private string _visitorCardInfo;
        private DateTime? _registerTime;
        private DateTime? _checkinTime;
        private bool? _leave;
        private DateTime? _leaveTime;
        private DateTime? _startDate;
        private DateTime? _endDate;


        public VisitInfoViewMolde()
        {
          
      
        }


        public VisitInfoViewMolde(DateTime _startDate, DateTime _endDate)
        {
            //_startDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 00:00"));
            //_endDate = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00"));


        }

        public VisitInfoViewMolde(VisitInfo info)
        {
            _vistorName = info.UserName ;
            _empName = info.EmpName;
            _visitorCardInfo = info.VisitorCardInfo;
            _registerTime = info.RegisterTime;
            _checkinTime = info.CheckinTime;
            _leave = info.Leave;
            _leaveTime = info.LeaveTime;
        }

        public string VisitorName
        {
            get { return _vistorName; }
            set
            {
                _vistorName = value;
                RaisePropertyChanged(() => VisitorName);
            }
        }

        public string EmployeeName
        {
            get { return _empName; }
            set
            {
                _empName = value;
                RaisePropertyChanged(() => EmployeeName);
            }
        }

        public string VisitorCardInfo
        {
            get { return _visitorCardInfo; }
            set
            {
                _visitorCardInfo = value;
                RaisePropertyChanged(() => VisitorCardInfo);
            }
        }

        public DateTime? RegisterTime
        {
            get { return _registerTime; }
            set
            {
                _registerTime = value;
                RaisePropertyChanged(() => RegisterTime);
            }
        }

        public DateTime? CheckinTime
        {
            get { return _checkinTime; }
            set
            {
                _checkinTime = value;
                RaisePropertyChanged(() => CheckinTime);
            }
        }


        public DateTime? StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                RaisePropertyChanged(() => StartDate);
            }
        }

        public DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                RaisePropertyChanged(() => EndDate);
            }
        }
        public bool? Leave
        {
            get { return _leave; }
            set
            {
                _leave = value;
                RaisePropertyChanged(() => Leave);
            }
        }

        public DateTime? LeaveTime
        {
            get { return _leaveTime; }
            set
            {
                _leaveTime = value;
                RaisePropertyChanged(() => LeaveTime);
            }
        }

    }
}
