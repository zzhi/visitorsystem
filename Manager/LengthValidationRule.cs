﻿
using System.Windows.Controls;

namespace VMS.Manager
{
    public class LengthValidationRule : ValidationRule
    {
        public int Length=0;
        public string Property = "";

        public LengthValidationRule()
        {
        }

        public LengthValidationRule(int length, string property)
        {
            Length=length;
            Property = property;
        }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {

            if (value == null)
            {
                return new ValidationResult(false, "内容不可以为空");
            }

            if (Property == "Identity" && value.ToString().Length != Length)
            {
                return new ValidationResult(false, "身份证号码有误");

            }
            if (Property == "UserName" && value.ToString().Length < Length)
            {
                return new ValidationResult(false, "访客姓名有误");
            }
            if (Property == "EmpName" && value.ToString().Length < Length)
            {
                return new ValidationResult(false, "被访者姓名有误");
            }

            return new ValidationResult(true, null);

        }
    }
}
