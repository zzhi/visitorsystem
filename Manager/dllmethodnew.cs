//write by ben
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;

namespace VMS.Manager
{
    class dllmethodnew
    {
        ///*打开设备*/
        //[DllImport("IDRCore.dll", EntryPoint = "Device_Open", CharSet = CharSet.Ansi)]
        //public static extern int Device_Open();


        [DllImport("IDRCore.dll", EntryPoint = "Device_Open", CharSet = CharSet.Ansi)]
        public static extern int Device_Open();


        /*关闭设备*/
        [DllImport("IDRCore.dll", EntryPoint = "Device_Close", CharSet = CharSet.Ansi)]
        public static extern int Device_Close();

        /*获取身份证BMP图像(OCR)(iCardType:1--第一代身份证 2--第二代身份证  
                                 szFileNameIn:返回身份证图片名称)*/
        [DllImport("IDRCore.dll", EntryPoint = "Get_IdcPic", CharSet = CharSet.Ansi)]
        public static extern int Get_IdcPic(int iCardType, string szFileNameIn);

        /*获取身份证BMP彩色图像(OCR)(iCardType:1--第一代身份证 2--第二代身份证  
                                     szFullFileNameOut:返回的身份证图片名称  
                                     szHeadFileNameOut:返回的人头像文件路径名)*/
        [DllImport("IDRCore.dll", EntryPoint = "Get_ColorPic", CharSet = CharSet.Ansi)]
        public static extern int Get_ColorPic(int iCardType, string szFullFileNameOut, string szHeadFileNameOut);

        /*获取身份证文字信息(OCR)(iCardType:1--第一代身份证 2--第二代身份证  
                                  szFileNameIn:返回身份证图片名称
                                  szFullFileNameOut:输出的图像文件路径名(校正后) 
                                  szHeadFileNameOut:输出的头像文件路径名  pstOut:文字信息)*/
        [DllImport("IDRCore.dll", EntryPoint = "Get_IdcData", CharSet = CharSet.Ansi)]
        public static extern int Get_IdcData(int iCardType, string szFileNameIn, string szFullFileNameOut, string szHeadFileNameOut, ref ID_CARD_S pstOut);

        /*获取身份证文字信息(OCR)*/
        [DllImport("IDRCore.dll", EntryPoint = "Get_IdcData2", CharSet = CharSet.Ansi)]
        public static extern int Get_IdcData2(int iCardType, string szFileNameIn, string szFullFileNameOut, string szHeadFileNameOut,ref TERMB_ITEM termb_item);

        /*机读二代证(芯片读取)*/
        [DllImport("IDRCore.dll", EntryPoint = "Get_TermbData", CharSet = CharSet.Ansi)]
        public static extern int Get_TermbData(string szFileNameIn, ref TERMB_ITEM termb_item);

        /*显示配置窗口*/
        [DllImport("IDRCore.dll", EntryPoint = "Show_ConfigWindow", CharSet = CharSet.Ansi)]
        public static extern int Show_ConfigWindow(IntPtr handle, int iCardTyp);

        /*显示错误消息*/
        [DllImport("IDRCore.dll", EntryPoint = "Format_ErrMsg", CharSet = CharSet.Ansi)]
        //public static unsafe extern int Format_ErrMsg(int iErrCodeIn, string* pErrMsgOut);
        public static extern int Format_ErrMsg(int iErrCodeIn, string pErrMsgOut);

        #region 二次调用后的函数

      //  获取身份证文字信息(二次调用)
        public static int Get_IdcData(int iCardType, string szFileNameIn, string szFullFileNameOut, string szHeadFileNameOut, ref IDCARD_ALL_SECOND pIdCard_All)
        {
            int nSuccFlag = -1;
            ID_CARD_S pId_Card = new ID_CARD_S();
            byte[] pText=new byte[256];
            string[] str = new string[10];
            for (int i = 0; i < 1; i++)
            {
                if (0 == Get_IdcData(iCardType, szFileNameIn, szFullFileNameOut, szHeadFileNameOut,   ref pId_Card))
                {
                    int j = 0,m=0;
                   
                       while(m<10)
                       {
                          Array.Copy(pId_Card.szText, j, pText, 0, 256);
                          str[m++]= System.Text.Encoding.Default.GetString(pText).Replace("\0","").Replace("?","");
                          
                           j = m* 255;
                        }
                   
                    nSuccFlag = 0;
                    pIdCard_All = new IDCARD_ALL_SECOND();  
                    pIdCard_All.name = str[1];      //姓名
                    pIdCard_All.sex = str[2];       //性别
                    pIdCard_All.people = str[4];    //民族
                    pIdCard_All.birthday = str[3];  //出生日期
                    pIdCard_All.address = str[7];   //住址
                    pIdCard_All.number = str[8];    //公民身份号码
                    pIdCard_All.validterm = "";                              //签发机关
                    pIdCard_All.startdate = str[5]; //有效期限(起始日期)
                    pIdCard_All.enddate = str[6];   //有效期限(截止日期)
                    pIdCard_All.newaddress = "";                             //最新住址
                    break;
                }

                Thread.Sleep(300);
            }

            return nSuccFlag;
        }

        //机读二代证(二次调用)
        public static int Get_TermbData(string szFileNameIn, ref IDCARD_ALL_SECOND pIdCard_All)
        {
            int nSuccFlag = -1;
            TERMB_ITEM termb_item = new TERMB_ITEM();
            byte[] pText = new byte[128];
            string[] str = new string[10];
            for (int i = 0; i < 1; i++)
            {
                if (0 == Get_TermbData(szFileNameIn, ref termb_item))
                {
                    int j = 0, m = 0;

                    while (m < 10)
                    {
                        Array.Copy(termb_item.pzTxt, j, pText, 0, 128);
                        str[m++] = System.Text.Encoding.Default.GetString(pText).Replace("\0", "").Replace("?", "");

                        j = m * 127;
                    }
                    nSuccFlag = 0;
                    pIdCard_All = new IDCARD_ALL_SECOND();
                    pIdCard_All.name = str[0];      //姓名
                    pIdCard_All.sex = str[1];       //性别
                    pIdCard_All.people = str[2];    //民族
                    pIdCard_All.birthday = str[3];  //出生日期
                    pIdCard_All.address = str[4];   //住址
                    pIdCard_All.number = str[5];    //公民身份号码
                    pIdCard_All.validterm = str[6]; //签发机关
                    pIdCard_All.startdate = str[7]; //有效期限(起始日期)
                    pIdCard_All.enddate = str[8];   //有效期限(截止日期)
                    pIdCard_All.newaddress = str[9];//最新住址
                    break;
                }

                Thread.Sleep(300);
            }

            return nSuccFlag;
        }

        //取二维数组中某序号的数组成字符串信息
        public static string GetTwoArray(byte[,] termb_item, int nIndexNo)
        {
            char[] pArray = new char[128];
            for (int i = 0; i < pArray.Length; i++)
            {
                pArray[i] = Convert.ToChar(termb_item[nIndexNo, i]);
            }

            return new string(pArray);
        }

        #endregion
    }

    /*结构ID_CARD_S
     是身份证图像识别结果，szText中存储的是文字结果，idRect是结果在整个图像中的位置。
     结构ID_CARD_S中，从szText[1]到szText[9]返回的分别为：姓名＼性别＼出生日期＼民族＼发证日期＼有效期限＼住址＼号码＼发证机关*/
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct HY_RECT_S
    {
        public long lLeft;
        public long lRight;
        public long lTop;
        public long lBottom;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct ID_CARD_S
    {
     [MarshalAs(UnmanagedType.ByValArray,SizeConst=2560)]
        public  byte[] szText;
     [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public HY_RECT_S[] idRect;
    }

    /*二代身份证芯片识别信息*/
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct TERMB_ITEM
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8960)]
        public byte[] pzTxt;
    }

    /*二代证机读信息(分解二维数组到各字段)*/
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct IDCARD_ALL_SECOND
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
        public string name;     //姓名
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string sex;      //性别
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string people;   //民族
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
        public string birthday; //出生日期
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string address;  //住址
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
        public string signdate;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
        public string validterm;//签发机关
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
        public string number;   //公民身份号码
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string other;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string szCodeOne;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string szCodeTwo;

        //新增属性
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string startdate; //有效期限(起始日期)
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string enddate;   //有效期限(结束日期)
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string newaddress;//最新住址
    }
}
