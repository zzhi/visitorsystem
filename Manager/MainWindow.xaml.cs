﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.VisualBasic;
using VMS.Common;
using VMS.Bll;
using VMS.Model;

namespace VMS.Manager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly RoutedCommand _changeLangs = new RoutedCommand("ChangeLanguage", typeof (MainWindow));

        private readonly RoutedCommand _addInfos = new RoutedCommand("AddInfos", typeof(MainWindow));


        private VisitorBll vbll = new VisitorBll();


        
        
        public MainWindow()
        {
            InitializeComponent();


            
            #region 语言切换

            //创建命令关联
            var cmd = new CommandBinding {Command = _changeLangs};
            cmd.CanExecute += ChangeLangs_CanExecute;
            cmd.Executed += ChangeLangs_Execute;
            //把命令关联安置在外围控件上
            this.CommandBindings.Add(cmd);
            //把命令赋值给命令源(发送者)
            miChinese.Command = _changeLangs;
            miEnglish.Command = _changeLangs;



            var cmdAddInfos = new CommandBinding { Command = _addInfos};
            cmdAddInfos.CanExecute += cmdAddInfos_CanExecute;
            cmdAddInfos.Executed += cmdAddInfos_Executed;
            this.CommandBindings.Add(cmdAddInfos);
            this.BtnAddInfos.Command = _addInfos;
           

            #endregion
           
        }

        void cmdAddInfos_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
         
            adTest();



        }


        private void adTest()
        {

            Report r = new Report();
            r.Show();
            //try
            //{
            //    Employee emp = this.VisitorCtl.GetEmployee();
            //    int empid = vbll.AddEmployee(emp);

            //    Visitor vtr = this.VisitorCtl.GetVisitor();
            //    int vtrid = vbll.AddVisitor(vtr);

            //    VisitInfo vInfo = new VisitInfo();
            //    vInfo.EmployeeId = empid;
            //    vInfo.VisitorId = vtrid;
            //    vInfo.VisitorCardInfo = "888888888888";
            //    vInfo.CheckinTime = null;
            //    vInfo.RegisterTime = DateTime.Now.AddHours(-2);
            //    vInfo.Leave = true;
            //    vInfo.LeaveTime = DateTime.Now;


            //    vbll.AddVisitInfo(vInfo);

            //    MessageBox.Show("添加成功");
            //    this.VisitorCtl.Clear();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }

        void cmdAddInfos_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }


        /// <summary>
        /// 语言切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeLangs_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            LangType lt = Langs.GetLanguage();

            Langs.SetLanguage(lt == LangType.ZhCn ? LangType.EnUs : LangType.ZhCn);
            e.Handled = true;
        }

        /// <summary>
        /// 语言切换检测
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeLangs_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            LangType lt = Langs.GetLanguage();
            var language = e.Parameter.ToString();
            e.CanExecute = lt.ToString() != language;

            e.Handled = true;
        }

    }
}