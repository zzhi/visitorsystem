﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using VMS.BLL;
//using VMS.ClientProxy;
//using VMS.ModelContract;
//using VMS.Common;

namespace VMS.Manager.UserControls
{
    /// <summary>
    /// Interaction logic for CardAuthorizationCtrl.xaml
    /// </summary>
    public partial class CardAuthorizationCtrl : UserControl
    {
        //public Super_User_InputModel model = new Super_User_InputModel();
        public CardAuthorizationCtrl()
        {
            InitializeComponent();
        }
        private void InitControl()
        {
            //List<string> accessGroupItems = AccgroupBLL.GetAccessGroup();
            //List<string> timeZoneItems = TimezoneBLL.GetTimezone();
            //cbAccessGroup.ItemsSource = accessGroupItems;
            //cbAccessGroup1.ItemsSource = accessGroupItems;
            //cbAccessGroup2.ItemsSource = accessGroupItems;
            //cbAccessGroup3.ItemsSource = accessGroupItems;

            //cbAccessGroup.SelectedIndex = 0;
            //cbAccessGroup1.SelectedIndex = 0;
            //cbAccessGroup2.SelectedIndex = 0;
            //cbAccessGroup3.SelectedIndex = 0;

            //cbTimeZone.ItemsSource = timeZoneItems;
            //cbTimeZone1.ItemsSource = timeZoneItems;
            //cbTimeZone2.ItemsSource = timeZoneItems;
            //cbTimeZone3.ItemsSource = timeZoneItems;

            //cbTimeZone.SelectedIndex = 0;
            //cbTimeZone1.SelectedIndex = 0;
            //cbTimeZone2.SelectedIndex = 0;
            //cbTimeZone3.SelectedIndex = 0;

            //cbTimeZone.IsEnabled = false;
            //cbTimeZone1.IsEnabled = false;
            //cbTimeZone2.IsEnabled = false;
            //cbTimeZone3.IsEnabled = false;
        }

        private void InitControlBindings()
        {
            //tbBadgeNumber.SetBinding(TextBox.TextProperty, new Binding("Badge") { Source = model });
            //cbAccessGroup.SetBinding(ComboBox.SelectedValueProperty, new Binding("AccessGrp") { Source = model });
            //cbAccessGroup1.SetBinding(ComboBox.SelectedValueProperty, new Binding("AccessGrp_01") { Source = model });
            //cbAccessGroup2.SetBinding(ComboBox.SelectedValueProperty, new Binding("AccessGrp_02") { Source = model });
            //cbAccessGroup3.SetBinding(ComboBox.SelectedValueProperty, new Binding("AccessGrp_03") { Source = model });

            //cbTimeZone.SetBinding(ComboBox.SelectedValueProperty, new Binding("TimeZone") { Source = model });
            //cbTimeZone1.SetBinding(ComboBox.SelectedValueProperty, new Binding("TimeZone_01") { Source = model });
            //cbTimeZone2.SetBinding(ComboBox.SelectedValueProperty, new Binding("TimeZone_02") { Source = model });
            //cbTimeZone3.SetBinding(ComboBox.SelectedValueProperty, new Binding("TimeZone_03") { Source = model });
        }

        private void cbAccessGroup_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //if (cbAccessGroup.SelectedIndex > 0)
            //{
            //    cbTimeZone.IsEnabled = true;
            //}
            //else
            //{
            //    cbTimeZone.IsEnabled = false;
            //}
            //if (cbAccessGroup.SelectedIndex == 0 || cbTimeZone.SelectedIndex == 0)
            //{
            //    model.AccessGrp = "null";
            //    model.TimeZone = "null";
            //}
        }

        private void cbAccessGroup1_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //if (cbAccessGroup1.SelectedIndex > 0)
            //{
            //    cbTimeZone1.IsEnabled = true;
            //}
            //else
            //{
            //    cbTimeZone1.IsEnabled = false;
            //}
            //if (cbAccessGroup1.SelectedIndex == 0 || cbTimeZone1.SelectedIndex == 0)
            //{
            //    model.AccessGrp_01 = "null";
            //    model.TimeZone_01 = "null";
            //}
        }

        private void cbAccessGroup2_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //if (cbAccessGroup2.SelectedIndex > 0)
            //{
            //    cbTimeZone2.IsEnabled = true;
            //}
            //else
            //{
            //    cbTimeZone2.IsEnabled = false;
            //}
            //if (cbAccessGroup2.SelectedIndex == 0 || cbTimeZone2.SelectedIndex == 0)
            //{
            //    model.AccessGrp_02 = "null";
            //    model.TimeZone_02 = "null";
            //}
        }

        private void cbAccessGroup3_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //if (cbAccessGroup3.SelectedIndex > 0)
            //{
            //    cbTimeZone3.IsEnabled = true;
            //}
            //else
            //{
            //    cbTimeZone3.IsEnabled = false;
            //}
            //if (cbAccessGroup3.SelectedIndex == 0 || cbTimeZone3.SelectedIndex == 0)
            //{
            //    model.AccessGrp_03 = "null";
            //    model.TimeZone_03 = "null";
            //}
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //InitControl();
            //InitControlBindings();
        }
    }
}
