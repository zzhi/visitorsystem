﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VMS.Bll;
using VMS.Model;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace VMS.Manager.UserControls
{
    /// <summary>
    /// Interaction logic for VisitListControl.xaml
    /// </summary>
    public partial class VisitListControl : UserControl
    {



        public VisitInfoListViewMolde vlistVm = new VisitInfoListViewMolde();

        public VisitInfoViewMolde vVm = new VisitInfoViewMolde() { StartDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 00:00")), EndDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 23:59")) };
        private readonly RoutedCommand _GetInfos = new RoutedCommand("GetInfos", typeof(VisitListControl));

        private readonly RoutedCommand _openReport = new RoutedCommand("OpenReport", typeof(VisitListControl));


        public VisitListControl()
        {
         
            InitializeComponent();
           
            this.DgList.DataContext = vlistVm;

          
            //查询信息
            var cmdGetInfos = new CommandBinding { Command = _GetInfos };
            cmdGetInfos.CanExecute += cmdAddInfos_CanExecute;
            cmdGetInfos.Executed += cmdAddInfos_Executed;
            this.CommandBindings.Add(cmdGetInfos);
            this.BtnGetInfos.Command = _GetInfos;


            //打开报表
            var cmdOpenReport = new CommandBinding { Command = _openReport};
            cmdOpenReport.CanExecute += cmdOpenReport_CanExecute;
            cmdOpenReport.Executed += cmdOpenReport_Executed;
            this.CommandBindings.Add(cmdOpenReport);
            this.BtnReport.Command = _openReport;



            //起始时间
            this.DtpSelStartDate.SetBinding(DateTimePicker.ValueProperty, new Binding("StartDate")
            {
                Mode = BindingMode.TwoWay,
                Source = vVm,
            });

            //至
            this.DtpSelEndDate.SetBinding(DateTimePicker.ValueProperty, new Binding("EndDate")
            {
                Mode=BindingMode.TwoWay,
                Source = vVm,
            });

         
            //访客
            this.TxtSelVisitorName.SetBinding(TextBox.TextProperty, new Binding("VisitorName")
            {
                Source = vVm,
            });
            
            //被访者
            this.TxtSelEmployeeName.SetBinding(TextBox.TextProperty, new Binding("EmployeeName")
            {
                Source = vVm,
            });

            
            //是否离开
            this.CbSelLeave.SetBinding(CheckBox.IsCheckedProperty, new Binding("Leave")
            {
                Source = vVm,
            });

           
         }

        void cmdOpenReport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            

            Report rpt = new Report();
            //rpt.vVm = vVm;
            rpt.vVm.EmployeeName = vVm.EmployeeName;
            rpt.vVm.EndDate = vVm.EndDate;
            rpt.vVm.Leave = vVm.Leave;
            rpt.vVm.StartDate = vVm.StartDate;
            rpt.vVm.VisitorName = vVm.VisitorName;
            rpt.ShowDialog();
        }

        void cmdOpenReport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        void cmdAddInfos_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            GetInfo();
        }

        void cmdAddInfos_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }


     

        public void GetInfo()
        {
            VisitorBll vbll = new VisitorBll();
            vlistVm.ClearAll();

            VisitInfo vinfo = new VisitInfo();
            vinfo.EmpName = vVm.EmployeeName;
            vinfo.UserName = vVm.VisitorName;
            vinfo.StartDate = vVm.StartDate;
            vinfo.EndDate = vVm.EndDate;
            vinfo.Leave = vVm.Leave;



            List<VisitInfo> infs = vbll.GetVisitInfo(vinfo);
            if (infs != null && infs.Count > 0)
            {
                foreach (VisitInfo info in infs)
                {
                    VisitInfoViewMolde vmode = new VisitInfoViewMolde(info);
                    vlistVm.AddItem(vmode);
                }
            }
        }
    }
}
