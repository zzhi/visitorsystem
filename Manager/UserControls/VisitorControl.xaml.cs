﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using VMS.Common;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;
using VMS.Model;
namespace VMS.Manager.UserControls
{
    /// <summary>
    /// Interaction logic for VisitorControl.xaml
    /// </summary>
    public partial class VisitorControl : UserControl
    {
        //public Visitor Visit;
        //public Employee Emp;

        public EmployeeViewModel EmpVm = new EmployeeViewModel();
        public VisitorViewModel VisitorVm = new VisitorViewModel();


        public VisitorControl()
        {
            InitializeComponent();

            InstantiationTest();

            
            #region 绑定访客身份证信息

            //用户名
            var bindUserName = new Binding("UserName")
            {
                Source = VisitorVm,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                NotifyOnValidationError = true,
            };
            bindUserName.ValidationRules.Add(new LengthValidationRule(2, "UserName")
            {
                ValidatesOnTargetUpdated = true
            });
            this.TxbUsername.SetBinding(TextBox.TextProperty, bindUserName);
            this.TxbUsername.AddHandler(Validation.ErrorEvent, new RoutedEventHandler(ValidationError));


            //身份证
            var bindIdentity = new Binding("Identity")
            {
                Source = VisitorVm,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                NotifyOnValidationError = true,
            };
            bindIdentity.ValidationRules.Add(new LengthValidationRule(18, "Identity")
            {
                ValidatesOnTargetUpdated = true
            });
            this.TxbIdentityCard.SetBinding(TextBox.TextProperty, bindIdentity);
            this.TxbIdentityCard.AddHandler(Validation.ErrorEvent, new RoutedEventHandler(ValidationError));



            //性别男
            this.RbtMale.SetBinding(ToggleButton.IsCheckedProperty, new Binding("SEX")
            {
                Source = VisitorVm,
                Converter = new SexToBoolConverter(),
                ConverterParameter = 0
            });


            //性别女
            this.RbtFemale.SetBinding(ToggleButton.IsCheckedProperty, new Binding("SEX")
            {
                Source = VisitorVm,
                Converter = new SexToBoolConverter(),
                ConverterParameter = 1
            });


            //证件照照片
            this.ImgIdentityPic.SetBinding(Image.SourceProperty, new Binding("IdentityPic")
            {
                Source = VisitorVm,
                Converter = new ByteArrayToBitmapImageConverter(),
                ConverterParameter = VisitorVm.IdentityPic
            });

            //证件图像
            this.ImgIdentityImg.SetBinding(Image.SourceProperty, new Binding("IdentityImg")
            {
                Source = VisitorVm,
                Converter = new ByteArrayToBitmapImageConverter(),
                ConverterParameter = VisitorVm.IdentityImg
            });



            //出生日期
            this.DtpBirthday.SetBinding(DateTimePicker.ValueProperty, new Binding("Birthday")
            {
                Source = VisitorVm,
            });

            //起始有效期
            this.DtpStartDate.SetBinding(DateTimePicker.ValueProperty, new Binding("StartDate")
            {
                Source = VisitorVm,
            });

            //有效期终止
            this.DtpEndDate.SetBinding(DateTimePicker.ValueProperty, new Binding("EndDate")
            {
                Source = VisitorVm,
            });


            //地址
            this.TxbAddress.SetBinding(TextBox.TextProperty, new Binding("Address")
            {
                Source = VisitorVm,
            });

            //备注
            this.TxbDescriptions.SetBinding(TextBox.TextProperty, new Binding("Descriptions")
            {
                Source = VisitorVm,
            });

            //民族
            this.TxbNationality.SetBinding(TextBox.TextProperty, new Binding("Nationality")
            {
                Source = VisitorVm,
            });

            #endregion

            #region 绑定访客其他信息

            //company
            this.TxbCompany.SetBinding(TextBox.TextProperty, new Binding("Company")
            {
                Source = VisitorVm,
            });
            //company adddress
            this.TxbCompanyAddress.SetBinding(TextBox.TextProperty, new Binding("CompanyAddress")
            {
                Source = VisitorVm,
            });

            //position
            this.TxbPosition.SetBinding(TextBox.TextProperty, new Binding("Position")
            {
                Source = VisitorVm,
            });

            //email
            this.TxbEmail.SetBinding(TextBox.TextProperty, new Binding("Email")
            {
                Source = VisitorVm,
            });

            //tel
            this.TxbTel.SetBinding(TextBox.TextProperty, new Binding("Tel")
            {
                Source = VisitorVm,
            });

            //Fax
            this.TxbFax.SetBinding(TextBox.TextProperty, new Binding("Fax")
            {
                Source = VisitorVm,
            });

            //post code
            this.TxbPostCode.SetBinding(TextBox.TextProperty, new Binding("PostCode")
            {
                Source = VisitorVm,
            });


            //描述1
            this.TxbDescriptions1.SetBinding(TextBox.TextProperty, new Binding("Descriptions1")
            {
                Source = VisitorVm,
            });


            //当前图片
            this.ImgCurrentImg.SetBinding(Image.SourceProperty, new Binding("CurrentImg")
            {
                Source = VisitorVm,
                Converter = new ByteArrayToBitmapImageConverter(),
                ConverterParameter = VisitorVm.CurrentImg
            });


            #endregion

            #region 绑定被访者


          
            // emp company
            this.TxbEmpCompany.SetBinding(TextBox.TextProperty, new Binding("Company")
            {
                Source = EmpVm,
            });
            //emp position
            this.TxbEmpPosition.SetBinding(TextBox.TextProperty, new Binding("Position")
            {
                Source = EmpVm,
            });

            //email
            this.TxbEmpEmail.SetBinding(TextBox.TextProperty, new Binding("Email")
            {
                Source = EmpVm,
            });

            //tel
            this.TxbEmpTel.SetBinding(TextBox.TextProperty, new Binding("Tel")
            {
                Source = EmpVm,
            });

            //emp Floor
            this.TxbEmpFloor.SetBinding(TextBox.TextProperty, new Binding("Floor")
            {
                Source = EmpVm,
            });



            //被访者
            var bindEmpName = new Binding("EmpName")
            {
                Source = EmpVm,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                NotifyOnValidationError = true,
            };
            bindEmpName.ValidationRules.Add(new LengthValidationRule(2, "EmpName")
            {
                ValidatesOnTargetUpdated = true,

            });
            this.TxbEmpName.SetBinding(TextBox.TextProperty, bindEmpName);
            this.TxbEmpName.AddHandler(Validation.ErrorEvent, new RoutedEventHandler(ValidationError));

         

            //emp Descriptions
            this.TxbEmpDescriptions.SetBinding(TextBox.TextProperty, new Binding("Descriptions")
            {
                Source = EmpVm,
            });


            #endregion
        }


      


        /// <summary>
        /// 验证提示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidationError(object sender, RoutedEventArgs e)
        {
            var txt = sender as TextBox;
            if (txt != null)
            {
                if (Validation.GetErrors(txt).Count > 0)
                {
                    txt.ToolTip = Validation.GetErrors(txt)[0].ErrorContent.ToString();
                }
                else
                {
                    txt.ToolTip = "";
                }
            }
        }


        public Employee GetEmployee()
        {
            var emp = new Employee();
            emp.Company = EmpVm.Company;
            emp.Descriptions = EmpVm.Descriptions;
            emp.Email = EmpVm.Email;
            emp.EmpName = EmpVm.EmpName;
            emp.Floor = EmpVm.Floor;
            emp.Position = EmpVm.Position;
            emp.Tel = EmpVm.Tel;
            return emp;
        }

        public void Clear()
        {
            EmpVm.Company = null;
            EmpVm.Descriptions = null;
            EmpVm.Email = null;
            EmpVm.EmpName = null;
            EmpVm.Floor = null;
            EmpVm.Position = null;
            EmpVm.Tel = null;


            VisitorVm.Identity = null;
            VisitorVm.UserName = null;
            VisitorVm.SEX = Sex.Female;
            VisitorVm.Nationality = null;
            VisitorVm.Birthday = null;
            VisitorVm.StartDate = null;
            VisitorVm.EndDate = null;
            VisitorVm.IdentityPic = null;
            VisitorVm.IdentityImg = null;
            VisitorVm.Descriptions = null;
            VisitorVm.Address = null;
            VisitorVm.Company = null;
            VisitorVm.CompanyAddress = null;
            VisitorVm.Position = null;
            VisitorVm.Email = null;
            VisitorVm.Tel = null;
            VisitorVm.PostCode = null;
            VisitorVm.Fax = null;
            VisitorVm.CurrentImg = null;
            VisitorVm.Descriptions1 = null;
           
        }

        public Visitor GetVisitor()
        {
            Visitor vis = new Model.Visitor();
            vis.Identity = VisitorVm.Identity;
            vis.UserName = VisitorVm.UserName;
            vis.SEX = VisitorVm.SEX;
            vis.Nationality = VisitorVm.Nationality;
            vis.Birthday = VisitorVm.Birthday;
            vis.StartDate = VisitorVm.StartDate;
            vis.EndDate = VisitorVm.EndDate;
            vis.IdentityPic = VisitorVm.IdentityPic;
            vis.IdentityImg = VisitorVm.IdentityImg;
            vis.Descriptions = VisitorVm.Descriptions;
            vis.Address = VisitorVm.Address;
            vis.Company = VisitorVm.Company;
            vis.CompanyAddress = VisitorVm.CompanyAddress;
            vis.Position = VisitorVm.Position;
            vis.Email = VisitorVm.Email;
            vis.Tel = VisitorVm.Tel;
            vis.PostCode = VisitorVm.PostCode;
            vis.Fax = VisitorVm.Fax;
            vis.CurrentImg = VisitorVm.CurrentImg;
            vis.Descriptions1 = VisitorVm.Descriptions1;
            return vis;
        }


        private void InstantiationTest()
        {
            VisitorVm = new VisitorViewModel()
            {
                UserName = "张智",
                Identity = "610198612160714",
                SEX = Sex.Female,
                
                Birthday = DateTime.Now,
                StartDate = DateTime.Now.AddYears(-5),
                EndDate = DateTime.Now.AddYears(5),
                Address = "北京市通州区马驹桥",
                Descriptions = "会谈",
                Nationality = "汉",
                Company = "johnson controls",
                CompanyAddress = "北京东城区祈年大街",
                Position = "manager",
                Email = "zhangzhi@hotmail.com",
                Tel = "15001328888",
                Fax = "98789",
                PostCode = "100000",
              
                Descriptions1 = "hello world",

                IdentityImg =
                    VMS.Common.Utils.GetPictureData(@"D:\default.jpg"),
                IdentityPic =
                    Utils.GetPictureData(@"D:\test1.jpg"),
                CurrentImg =
                    Utils.GetPictureData(@"D:\test2.jpg")
            };
            EmpVm =new EmployeeViewModel ()
            {
                EmpName = "阿呆",
                Company = "微软",
                Position = "Dev",
                Email = "zhi@gmail.com",
                Tel = "010-59281975",
                Floor = "12F",
                Descriptions = "Hi"
            };
        }
    }
}
