﻿using System;
using System.Collections.ObjectModel;

namespace VMS.Manager
{
    [Serializable]
    public class VisitorList : ViewModelBase
    {

        ObservableCollection<VisitorViewModel> _visitorItems = new ObservableCollection<VisitorViewModel> { };

        public ObservableCollection<VisitorViewModel> VisitorItems
        {
            get { return _visitorItems; }
            set
            {
                _visitorItems = value;
                RaisePropertyChanged(() => VisitorItems);
            }
        }


        public void AddItem(VisitorViewModel s)
        {
            _visitorItems.Add(s);
        }

        public void ClearAll()
        {
            _visitorItems.Clear();
        }

    }
}