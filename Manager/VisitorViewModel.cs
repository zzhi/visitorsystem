﻿using System;
using VMS.Common;
using VMS.Model;

namespace VMS.Manager
{
    [Serializable]
    public class VisitorViewModel : ViewModelBase
    {
        private DateTime? _birthday;
        private DateTime? _endDate;
        private string _identity;
        private string _nationality;
        private Sex _sex;
        private DateTime? _startDate;
        private string _username;
        private byte[] _identityPic;
        private byte[] _identityImg;
        private string _descriptions;
        private string _address;


        private string _company;
        private string _companyaddress;
        private string _position;
        private string _email;
        private string _tel;
        private string _postcode;
        private string _fax ;
        private byte[] _currentImg ;
        private string _descriptions1;


        public VisitorViewModel()
        {
        }

        public VisitorViewModel(Visitor v)
        {
            _username = v.UserName;
            _identity = v.Identity;
            _sex = v.SEX;

            _birthday = v.Birthday;
            _startDate = v.StartDate;
            _endDate = v.EndDate;

            _address = v.Address;
            _nationality = v.Nationality;
            _descriptions = v.Descriptions;

            _identityPic = v.IdentityPic;
            _identityImg = v.IdentityImg;
            _nationality = v.Nationality;



            _company = v.Company;
            _companyaddress = v.CompanyAddress;
            _position = v.Position;
            _email = v.Email;
            _tel = v.Tel;
            _postcode = v.PostCode;
            _fax = v.Fax;
            _currentImg = v.CurrentImg;
            _descriptions1 = v.Descriptions1;

        }

        /// <summary>
        ///     身份证号
        /// </summary>
        public string Identity
        {
            get { return _identity; }
            set
            {
                _identity = value;
                RaisePropertyChanged(() => Identity);
            }
        }


        /// <summary>
        ///     姓名
        /// </summary>
        public string UserName
        {
            get { return _username; }
            set
            {
                _username = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        /// <summary>
        ///     性别
        /// </summary>
        public Sex SEX
        {
            get { return _sex; }
            set
            {
                _sex = value;
                RaisePropertyChanged(() => SEX);
            }
        }

        /// <summary>
        ///     民族
        /// </summary>
        public string Nationality
        {
            get { return _nationality; }
            set
            {
                _nationality = value;
                RaisePropertyChanged(() => Nationality);
            }
        }

        /// <summary>
        ///     出生日期
        /// </summary>
        public DateTime? Birthday
        {
            get { return _birthday; }
            set
            {
                _birthday = value;
                RaisePropertyChanged(() => Birthday);
            }
        }


        /// <summary>
        ///     有效期开始时间
        /// </summary>
        public DateTime? StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                RaisePropertyChanged(() => StartDate);
            }
        }

        /// <summary>
        ///  有效期结束时间
        /// </summary>
        public DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                RaisePropertyChanged(() => EndDate);
            }
        }

        /// <summary>
        /// 证件照片
        /// </summary>
        public byte[] IdentityPic
        {
            get { return _identityPic; }
            set
            {
                _identityPic = value;
                RaisePropertyChanged(() => IdentityPic);
            }
        }

        /// <summary>
        /// 证件图像
        /// </summary>
        public byte[] IdentityImg
        {
            get { return _identityImg; }
            set
            {
                _identityImg = value;
                RaisePropertyChanged(() => IdentityImg);
            }
        }


        /// <summary>
        /// 备注
        /// </summary>
        public String Descriptions
        {
            get { return _descriptions; }
            set
            {
                _descriptions = value;
                RaisePropertyChanged(() => Descriptions);
            }
        }

        /// <summary>
        /// 地址
        /// </summary>
        public String Address
        {
            get { return _address; }
            set
            {
                _address = value;
                RaisePropertyChanged(() => Address);
            }
        }

        #region other infos
        public String Company
        {
            get { return _company; }
            set
            {
                _company = value;
                RaisePropertyChanged(() => Company);
            }
        }

        public String CompanyAddress
        {
            get {  return _companyaddress; }
            set
            {
                _companyaddress = value;
                RaisePropertyChanged(() => CompanyAddress);
            }
        }

        public String Position
        {
            get { return _position; }
            set
            {
                _position = value;
                RaisePropertyChanged(() => Position);
            }
        }



        public String Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged(() => Email);
            }
        }

        public String Tel
        {
            get { return _tel; }
            set
            {
                _tel = value;
                RaisePropertyChanged(() => Tel);
            }
        }


        public String PostCode
        {
            get { return _postcode; }
            set
            {
                _postcode = value;
                RaisePropertyChanged(() => PostCode);
            }
        }


        public String Fax
        {
            get { return _fax; }
            set
            {
                _fax = value;
                RaisePropertyChanged(() => Fax);
            }
        }


        public byte[] CurrentImg
        {
            get { return _currentImg; }
            set
            {
                _currentImg = value;
                RaisePropertyChanged(() => CurrentImg);
            }
        }


        /// <summary>
        /// 备注1
        /// </summary>
        public String Descriptions1
        {
            get { return _descriptions1; }
            set
            {
                _descriptions1 = value;
                RaisePropertyChanged(() => Descriptions1);
            }
        }
        #endregion


        #region INotifyPropertyChanged Members

        //[field: NonSerialized]
        //public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        //private void NotifyPropertyChanged(string propertyName)
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}

        #endregion
    }
}