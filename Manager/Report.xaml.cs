﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using VMS.Bll;
using VMS.Model;
using Xceed.Wpf.Toolkit;

namespace VMS.Manager
{
    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class Report : Window
    {



        public VisitInfoViewMolde vVm = new VisitInfoViewMolde() { StartDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 00:00")), EndDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 23:59")) };
        private readonly RoutedCommand _GetInfos = new RoutedCommand("GetInfos", typeof(Report));

        private  VisitorBll vbll = new VisitorBll();
        public Report()
        {
            InitializeComponent();
            //查询信息
            var cmdGetInfos = new CommandBinding { Command = _GetInfos };
            cmdGetInfos.CanExecute += cmdCanGetInfos_CanExecute;
            cmdGetInfos.Executed += cmdGetInfos_Executed;
            this.CommandBindings.Add(cmdGetInfos);
            this.BtnGetInfos.Command = _GetInfos;




            //起始时间
            this.DtpSelStartDate.SetBinding(DateTimePicker.ValueProperty, new Binding("StartDate")
            {
                Mode = BindingMode.TwoWay,
                Source = vVm,
            });

            //至
            this.DtpSelEndDate.SetBinding(DateTimePicker.ValueProperty, new Binding("EndDate")
            {
                Mode = BindingMode.TwoWay,
                Source = vVm,
            });


            //访客
            this.TxtSelVisitorName.SetBinding(TextBox.TextProperty, new Binding("VisitorName")
            {
                Source = vVm,
            });

            //被访者
            this.TxtSelEmployeeName.SetBinding(TextBox.TextProperty, new Binding("EmployeeName")
            {
                Source = vVm,
            });


            //是否离开
            this.CbSelLeave.SetBinding(CheckBox.IsCheckedProperty, new Binding("Leave")
            {
                Source = vVm,
            });


          

        }




        private void GetInfo()
        {

            VisitInfo vinfo = new VisitInfo();
            vinfo.EmpName = vVm.EmployeeName;
            vinfo.UserName = vVm.VisitorName;
            vinfo.StartDate = vVm.StartDate;
            vinfo.EndDate = vVm.EndDate;
            vinfo.Leave = vVm.Leave;

            var dt = vbll.GetVisitReportInfo(vinfo);

            ReportDataSource reportDataSource1;
            _reportViewer.ProcessingMode = ProcessingMode.Local;
            if (dt != null)
            {
                reportDataSource1 = new ReportDataSource {Name = "VisitInfo", Value = dt};
            }
            else
            {
                reportDataSource1 = new ReportDataSource { Name = "VisitInfo", Value = new VMS.Manager.DataSet.VisitInfoDataTable() };
           
            }
            _reportViewer.LocalReport.DataSources.Clear();
            _reportViewer.LocalReport.DataSources.Add(reportDataSource1);
            _reportViewer.LocalReport.ReportPath = @AppDomain.CurrentDomain.BaseDirectory + "\\VisitReport.rdlc";

            _reportViewer.RefreshReport();
        }



        void cmdGetInfos_Executed(object sender, ExecutedRoutedEventArgs e)
        {
           

            VisitInfo vinfo = new VisitInfo();
            vinfo.EmpName = vVm.EmployeeName;
            vinfo.UserName = vVm.VisitorName;
            vinfo.StartDate = vVm.StartDate;
            vinfo.EndDate = vVm.EndDate;
            vinfo.Leave = vVm.Leave;

            var dt = vbll.GetVisitReportInfo(vinfo);
            

            _reportViewer.ProcessingMode = ProcessingMode.Local;

            var reportDataSource1 = new ReportDataSource { Name = "VisitInfo", Value = dt };
            _reportViewer.LocalReport.DataSources.Clear();
            _reportViewer.LocalReport.DataSources.Add(reportDataSource1);
            _reportViewer.LocalReport.ReportPath = @AppDomain.CurrentDomain.BaseDirectory + "\\VisitReport.rdlc";

            _reportViewer.RefreshReport();
        }

        void cmdCanGetInfos_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetInfo();
        }



    }
}
