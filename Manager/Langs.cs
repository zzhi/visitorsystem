﻿using System;
using System.Windows;

namespace VMS.Manager
{
    public enum LangType
    {
        EnUs,
        ZhCn
    }

    public class Langs
    {
        private static string _currentLanguageFile;

        public static LangType GetLanguage()
        {
            return _currentLanguageFile == "/Resources/Langs/zh-CN.xaml" ? LangType.ZhCn : LangType.EnUs;
        }

        public static void SetLanguage(LangType lt)
        {
            _currentLanguageFile = string.Format("/Resources/Langs/{0}.xaml", lt == LangType.ZhCn ? "zh-CN" : "en-US");
            LoadLanguage(_currentLanguageFile);
        }

        /// <summary>
        /// According to the resource file to switch the language used in the current application
        /// </summary>
        /// <param name="langFile"></param>
        private static void LoadLanguage(string langFile)
        {
            var rd = new ResourceDictionary() { Source = new Uri(langFile, UriKind.RelativeOrAbsolute) };

            if (Application.Current.Resources.MergedDictionaries.Count == 0)
            {
                Application.Current.Resources.MergedDictionaries.Add(rd);
            }
            else
            {
                Application.Current.Resources.MergedDictionaries[0] = rd;
            }
        }
    }
}