﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Model;
using VMS.DBUtility;
namespace VMS.DAL

{
    public class VisitorDal
    {

        public Employee GetEmployee(int id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 id,EmpName,Company,Position,Email,Tel,Floor,Descriptions from VMS_Employee ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int)
            };
            parameters[0].Value = id;

            var emp = new Employee();

            DataSet ds = VMS.DBUtility.DbHelperSql.Query(strSql.ToString(), parameters);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        public int AddEmployee(Employee emp)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(
                " INSERT INTO [VMS_Employee] ([EmpName],[Company],[Position] ,[Email] ,[Tel],[Floor],[Descriptions])");
            strSql.Append(" VALUES");
            strSql.Append(" (@EmpName,@Company,@Position,@Email,@Tel,@Floor,@Descriptions);");
            strSql.Append(" select @id=@@identity;");
            SqlParameter[] parameters =
            {
                new SqlParameter("@EmpName", SqlDbType.NVarChar, 50),
                new SqlParameter("@Company", SqlDbType.NVarChar, 100),
                new SqlParameter("@Position", SqlDbType.NVarChar, 100),
                new SqlParameter("@Email", SqlDbType.NVarChar, 200),
                new SqlParameter("@Tel", SqlDbType.NVarChar, 20),
                new SqlParameter("@Floor", SqlDbType.NVarChar, 20),
                new SqlParameter("@Descriptions", SqlDbType.NVarChar, 500),
                new SqlParameter("@id", SqlDbType.Int, 8)
            };
            parameters[0].Value = emp.EmpName;
            parameters[1].Value = emp.Company;
            parameters[2].Value = emp.Position;
            parameters[3].Value = emp.Email ;
            parameters[4].Value = emp.Tel;
            parameters[5].Value = emp.Floor;
            parameters[6].Value = emp.Descriptions;
            parameters[7].Direction = ParameterDirection.Output;
            try
            {
                VMS.DBUtility.DbHelperSql.ExecuteSql(strSql.ToString(), parameters);
                string id = parameters[7].Value.ToString();
                return Convert.ToInt32(id);
            }
            catch (Exception ex)
            {
                Common.Logs.jobError.Info(this.ToString() + ".AddEmployee:" + ex.Message);
                return 0;

            }

        }



        public void AddVisitInfo(VisitInfo vInfo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(
                " INSERT INTO [VMS_VisitInfo] ([VisitorId],[EmployeeId],[VisitorCardInfo] ,[RegisterTime],[CheckinTime],[Leave],[LeaveTime])");
            strSql.Append(" VALUES");
            strSql.Append(" (@VisitorId,@EmployeeId,@VisitorCardInfo,@RegisterTime,@CheckinTime,@Leave,@LeaveTime);");

            SqlParameter[] parameters =
            {
                new SqlParameter("@VisitorId", SqlDbType.Int),
                new SqlParameter("@EmployeeId", SqlDbType.Int),
                new SqlParameter("@VisitorCardInfo", SqlDbType.NVarChar, 100),
                new SqlParameter("@RegisterTime", SqlDbType.DateTime),
                new SqlParameter("@CheckinTime", SqlDbType.DateTime),
                new SqlParameter("@Leave", SqlDbType.Bit),
                new SqlParameter("@LeaveTime", SqlDbType.DateTime)

            };
            parameters[0].Value = vInfo.VisitorId;
            parameters[1].Value = vInfo.EmployeeId;
            parameters[2].Value = vInfo.VisitorCardInfo;
            parameters[3].Value = vInfo.RegisterTime;
            parameters[4].Value = vInfo.CheckinTime;
            parameters[5].Value = vInfo.Leave == true ? 1 : 0;
            parameters[6].Value = vInfo.LeaveTime;

            try
            {
                VMS.DBUtility.DbHelperSql.ExecuteSql(strSql.ToString(), parameters);

            }
            catch (Exception ex)
            {
                Common.Logs.jobError.Info(this.ToString() + ".AddVisitInfo:" + ex.Message);


            }

        }


        public int AddVisitor(Visitor vtr)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" INSERT INTO [VMS_Visitor]([Identity],[UserName],[SEX],[Nationality],[Birthday]");
            strSql.Append(" ,[StartDate] ,[EndDate],[IdentityPic] ,[IdentityImg] ,[Descriptions] ,[Address] ");
            strSql.Append(" ,[Company] ,[CompanyAddress] ,[Position] ,[Email] ,[Tel],[PostCode],[Fax] ,[CurrentImg] ,[Descriptions1])");
            strSql.Append(" VALUES");
            strSql.Append(" (@Identity,@UserName,@SEX,@Nationality,@Birthday");
            strSql.Append(" ,@StartDate,@EndDate,@IdentityPic ,@IdentityImg ,@Descriptions,@Address");
            strSql.Append(" ,@Company,@CompanyAddress,@Position,@Email,@Tel,@PostCode,@Fax,@CurrentImg,@Descriptions1);");
            strSql.Append(" select @id=@@identity;");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Identity", SqlDbType.NVarChar, 50),
                new SqlParameter("@UserName", SqlDbType.NVarChar, 20),
                new SqlParameter("@SEX", SqlDbType.Bit),
                new SqlParameter("@Nationality", SqlDbType.NVarChar, 20),
                new SqlParameter("@Birthday", SqlDbType.Date),

                new SqlParameter("@StartDate", SqlDbType.Date),
                new SqlParameter("@EndDate", SqlDbType.Date),
                new SqlParameter("@IdentityPic", SqlDbType.Image),
                new SqlParameter("@IdentityImg", SqlDbType.Image),
                new SqlParameter("@Descriptions", SqlDbType.NVarChar, 500),

                new SqlParameter("@Address", SqlDbType.NVarChar, 200),
                new SqlParameter("@Company", SqlDbType.NVarChar, 100),
                new SqlParameter("@CompanyAddress", SqlDbType.NVarChar, 200),
                new SqlParameter("@Position", SqlDbType.NVarChar, 50),
                new SqlParameter("@Email", SqlDbType.NVarChar, 100),

                new SqlParameter("@Tel", SqlDbType.NVarChar, 20),
                new SqlParameter("@PostCode", SqlDbType.NVarChar, 20),
                new SqlParameter("@Fax", SqlDbType.NVarChar, 20),
                new SqlParameter("@CurrentImg", SqlDbType.Image),
                new SqlParameter("@Descriptions1", SqlDbType.NVarChar, 500),

                new SqlParameter("@id", SqlDbType.Int, 8)
            };

            parameters[0].Value = vtr.Identity;
            parameters[1].Value = vtr.UserName;
            parameters[2].Value = (int)vtr.SEX;
            parameters[3].Value = vtr.Nationality;
            parameters[4].Value = vtr.Birthday;

            parameters[5].Value = vtr.StartDate;
            parameters[6].Value = vtr.EndDate;
            parameters[7].Value = vtr.IdentityPic;
            parameters[8].Value = vtr.IdentityImg;
            parameters[9].Value = vtr.Descriptions;
            parameters[10].Value = vtr.Address;

            parameters[11].Value = vtr.Company;
            parameters[12].Value = vtr.Birthday;
            parameters[13].Value = vtr.CompanyAddress;
            parameters[14].Value = vtr.Position;
            parameters[15].Value = vtr.Email;
            parameters[16].Value = vtr.Tel;
            parameters[17].Value = vtr.PostCode;
            parameters[18].Value = vtr.CurrentImg;
            parameters[19].Value = vtr.Descriptions1;
   
            parameters[20].Direction = ParameterDirection.Output;
            try
            {
                VMS.DBUtility.DbHelperSql.ExecuteSql(strSql.ToString(), parameters);
                string id = parameters[20].Value.ToString();


                return Convert.ToInt32(id);
            }
            catch (Exception ex)
            {

                Common.Logs.jobError.Info(this.ToString() + ".AddVisitor:"+ex.Message);
                return 0;

            }

        }
    

        public void test()
        {

            //创建SQL语句 
            string sql = "insert into test1 (id,img) values (@id,@img)";

            Byte[] bytBLOBData =VMS.Common.Utils.GetPictureData(@"D:\WorkSpace\Seed\visitorsystem\bin\Debug\IdentityPics\default.jpg");

            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int),
                new SqlParameter("@img", SqlDbType.Image, bytBLOBData.Length,
                    ParameterDirection.Input, true, 0, 0, null, DataRowVersion.Default, bytBLOBData)
            };
            parameters[0].Value = 1;
            int a = VMS.DBUtility.DbHelperSql.ExecuteSql(sql, parameters);

        }

        public Employee DataRowToModel(DataRow row)
        {
            var model = new Employee();
            if (row == null) return model;
            if (row["id"] != null && row["id"].ToString() != "")
            {
                model.Id = int.Parse(row["id"].ToString());
            }
            if (row["EmpName"] != null)
            {
                model.EmpName = row["EmpName"].ToString();
            }
            if (row["Company"] != null)
            {
                model.Company = row["Company"].ToString();
            }
            if (row["Position"] != null && row["Position"].ToString() != "")
            {
                model.Position =row["Position"].ToString();
            }
            if (row["Email"] != null && row["Email"].ToString() != "")
            {
                model.Email = row["Email"].ToString();
            }
            if (row["Tel"] != null && row["Tel"].ToString() != "")
            {
                model.Tel =row["Tel"].ToString();
            }
            if (row["Floor"] != null && row["Floor"].ToString() != "")
            {
                model.Floor = row["Floor"].ToString();
            }
            if (row["Descriptions"] != null && row["Descriptions"].ToString() != "")
            {
                model.Descriptions = row["Descriptions"].ToString();
            }
            return model;
        }



        public List<VisitInfo> GetVisitInfo()
        {

            List<VisitInfo> list = new List<VisitInfo>();
            StringBuilder sql = new StringBuilder();

            sql.Append(
                "SELECT [VisitorId],[EmployeeId],[RegisterTime] ,[CheckinTime],[Leave],[LeaveTime],vtr.username Username,emp.empname EmpName");
            sql.Append(" FROM [VMS_VisitInfo] as vif inner join vms_visitor as vtr");
            sql.Append(" on vif.visitorid=vtr.id");
            sql.Append(" inner join vms_employee emp ");
            sql.Append(" on vif.employeeid=emp.id");
            DataSet ds = VMS.DBUtility.DbHelperSql.Query(sql.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var vf = new VisitInfo();
                    vf.VisitorId = int.Parse(ds.Tables[0].Rows[i]["VisitorId"].ToString());
                    vf.EmployeeId = int.Parse(ds.Tables[0].Rows[i]["EmployeeId"].ToString());
                    vf.RegisterTime = DateTime.Parse(ds.Tables[0].Rows[i]["RegisterTime"].ToString());
                    vf.Leave = ds.Tables[0].Rows[i]["Leave"].ToString().ToLower() == "false" ? false: true;
                    if (String.IsNullOrEmpty(ds.Tables[0].Rows[i]["CheckinTime"].ToString()))
                    {

                        vf.CheckinTime = null;
                    }
                    else
                    {
                        vf.CheckinTime = DateTime.Parse(ds.Tables[0].Rows[i]["CheckinTime"].ToString());
                    }


                    if (String.IsNullOrEmpty(ds.Tables[0].Rows[i]["LeaveTime"].ToString()))
                    {

                        vf.LeaveTime = null;
                    }
                    else
                    {
                        vf.LeaveTime = DateTime.Parse(ds.Tables[0].Rows[i]["LeaveTime"].ToString());
                    }
                 
                    vf.UserName = ds.Tables[0].Rows[i]["UserName"].ToString();
                    vf.EmpName = ds.Tables[0].Rows[i]["EmpName"].ToString();
                    list.Add(vf);

                }
            }
            else
            {
                return null;
            }

            return list;
        }




        public List<VisitInfo> GetVisitInfo(VisitInfo vinfo)
        {
            List<VisitInfo> list = new List<VisitInfo>();
            StringBuilder sql = new StringBuilder();

            sql.Append(
                "SELECT [VisitorId],[EmployeeId],[RegisterTime] ,[CheckinTime],[Leave],[LeaveTime],vtr.username Username,emp.empname EmpName");
            sql.Append(" FROM [VMS_VisitInfo] as vif inner join vms_visitor as vtr");
            sql.Append(" on vif.visitorid=vtr.id");
            sql.Append(" inner join vms_employee emp ");
            sql.Append(" on vif.employeeid=emp.id");
            sql.Append(" where 1=1 ");

            if (vinfo != null)
            {
                if (!string.IsNullOrEmpty(vinfo.UserName))
                {
                    sql.Append(" and vtr.username like '%"+vinfo.UserName+"%'");
                }

                if (!string.IsNullOrEmpty(vinfo.EmpName))
                {
                    sql.Append(" and emp.empname like '%" + vinfo.EmpName + "%'");
                }

                if (vinfo.StartDate!=null)
                {
                    sql.Append(" and vif.RegisterTime >='" + vinfo.StartDate+"'");
                }

                if (vinfo.EndDate != null)
                {
                    sql.Append(" and vif.RegisterTime <='" + vinfo.EndDate + "'");
                }

                if (vinfo.Leave != null)
                {
                    int i = 0;
                    if ((bool) (vinfo.Leave = true))
                    {
                        i = 1;
                    }
                    sql.Append(" and vif.Leave=" + i);

                }


            }
          

            DataSet ds = VMS.DBUtility.DbHelperSql.Query(sql.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var vf = new VisitInfo();
                    vf.VisitorId = int.Parse(ds.Tables[0].Rows[i]["VisitorId"].ToString());
                    vf.EmployeeId = int.Parse(ds.Tables[0].Rows[i]["EmployeeId"].ToString());
                    vf.RegisterTime = DateTime.Parse(ds.Tables[0].Rows[i]["RegisterTime"].ToString());
                    vf.Leave = ds.Tables[0].Rows[i]["Leave"].ToString().ToLower() == "false" ? false : true;
                    if (String.IsNullOrEmpty(ds.Tables[0].Rows[i]["CheckinTime"].ToString()))
                    {

                        vf.CheckinTime = null;
                    }
                    else
                    {
                        vf.CheckinTime = DateTime.Parse(ds.Tables[0].Rows[i]["CheckinTime"].ToString());
                    }


                    if (String.IsNullOrEmpty(ds.Tables[0].Rows[i]["LeaveTime"].ToString()))
                    {

                        vf.LeaveTime = null;
                    }
                    else
                    {
                        vf.LeaveTime = DateTime.Parse(ds.Tables[0].Rows[i]["LeaveTime"].ToString());
                    }

                    vf.UserName = ds.Tables[0].Rows[i]["UserName"].ToString();
                    vf.EmpName = ds.Tables[0].Rows[i]["EmpName"].ToString();
                    list.Add(vf);

                }
            }
            else
            {
                return null;
            }

            return list;
        }




        public DataTable GetVisitReportInfo(VisitInfo vinfo)
        {

            List<VisitInfo> list = new List<VisitInfo>();
            StringBuilder sql = new StringBuilder();

            sql.Append(
                "SELECT [VisitorId],[EmployeeId],[RegisterTime] ,[CheckinTime],[Leave],[LeaveTime],vtr.username Username,emp.empname EmpName");
            sql.Append(" FROM [VMS_VisitInfo] as vif inner join vms_visitor as vtr");
            sql.Append(" on vif.visitorid=vtr.id");
            sql.Append(" inner join vms_employee emp ");
            sql.Append(" on vif.employeeid=emp.id");
            sql.Append(" where 1=1 ");

            if (vinfo != null)
            {
                if (!string.IsNullOrEmpty(vinfo.UserName))
                {
                    sql.Append(" and vtr.username like '%" + vinfo.UserName + "%'");
                }

                if (!string.IsNullOrEmpty(vinfo.EmpName))
                {
                    sql.Append(" and emp.empname like '%" + vinfo.EmpName + "%'");
                }

                if (vinfo.StartDate != null)
                {
                    sql.Append(" and vif.RegisterTime >='" + vinfo.StartDate + "'");
                }

                if (vinfo.EndDate != null)
                {
                    sql.Append(" and vif.RegisterTime <='" + vinfo.EndDate + "'");
                }

                if (vinfo.Leave != null)
                {
                    int i = 0;
                    if ((bool) (vinfo.Leave = true))
                    {
                        i = 1;
                    }
                    sql.Append(" and vif.Leave=" + i);

                }


            }


            DataSet ds = VMS.DBUtility.DbHelperSql.Query(sql.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null ;
        }

    }
}
