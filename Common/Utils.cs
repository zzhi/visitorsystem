﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Net.NetworkInformation;

namespace VMS.Common
{
    public class Utils
    {
        public  static bool  CreateProcess(string fileName, string arguments)
        {
            
            try
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                Process process = new Process();
                psi.FileName = fileName;
                psi.Arguments = arguments;
                process.StartInfo = psi;
                process.Start();

                Logs.jobStatus.Info(fileName + ":" + "启动");
                return true;
            }
            catch (Exception ex)
            {
                Logs.jobError.Info(fileName+":"+ex.Message);
                return false;
            }

        }


        public static byte[] GetPictureData(string imagepath)
        {

            var fs = new FileStream(imagepath, FileMode.Open);
            var byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            return byData;
        }


    }
}
