﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Common
{
    public enum Enumtbl_point_data_type
    {
        Unknown,
        Analog,
        Digital,
        Boolean
    }

    public enum Sex
    {
        Male,
        Female
    }

}
