﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace VMS.Common
{
    /// <summary>
    /// Operation Log class
    /// </summary>
    public  class Logs
    {
        /// <summary>
        /// status log 
        /// </summary>
        public  static ILog jobStatus = LogManager.GetLogger("Job.Status");

        /// <summary>
        /// error log
        /// </summary>
        public static ILog jobError = LogManager.GetLogger("Job.Error");

    }
}
