﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.DAL;
using VMS.Model;

namespace VMS.Bll
{
    public class VisitorBll
    {
        private VisitorDal dal = new VisitorDal();
        public Employee GetEmployee(int id)
        {
            return dal.GetEmployee(1);
        }

        public int AddEmployee(Employee emp)
        {
            return dal.AddEmployee(emp);
        }

        public List<VisitInfo> GetVisitInfo()
        {
            List<VisitInfo> list = dal.GetVisitInfo();

            return list;
        }


        public List<VisitInfo> GetVisitInfo(VisitInfo vinfo)
        {
            List<VisitInfo> list = dal.GetVisitInfo(vinfo);

            return list;
        }

        public int AddVisitor(Visitor vtr)
        {
            return dal.AddVisitor(vtr);
        }



        public void AddVisitInfo(VisitInfo vInfo)
        {
            dal.AddVisitInfo(vInfo);
        }

        public void test()
        {
            dal.test();
        }


        public DataTable GetVisitReportInfo(VisitInfo vinfo)
        {
            return dal.GetVisitReportInfo(vinfo);
        }
    }
}
