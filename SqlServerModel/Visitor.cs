﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Common;

namespace VMS.Model
{
    public class Visitor : ModelBase
    {

        public Visitor()
        {
        }

        /// <summary>
        ///     身份证号
        /// </summary>
        public string Identity { get; set; }


        /// <summary>
        ///     姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        ///     性别
        /// </summary>
        public Sex SEX { get; set; }

        /// <summary>
        ///     民族
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        ///     出生日期
        /// </summary>
        public DateTime? Birthday { get; set; }


        /// <summary>
        ///     有效期开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        ///  有效期结束时间
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// 证件照片
        /// </summary>
        public byte[] IdentityPic { get; set; }

        /// <summary>
        /// 证件图像
        /// </summary>
        public byte[] IdentityImg { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        public String Descriptions { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public String Address { get; set; }



        #region other infos
        public String Company
        {
            get;
            set;
        }

        public String CompanyAddress
        {
            get;
            set;
        }

        public String Position
        {
            get;
            set;
        }



        public String Email
        {
            get;
            set;
        }

        public String Tel
        {
            get;
            set;
        }


        public String PostCode
        {
            get;
            set;
        }


        public String Fax
        {
            get;
            set;
        }


        public byte[] CurrentImg
        {
            get;
            set;
        }


        /// <summary>
        /// 备注1
        /// </summary>
        public String Descriptions1
        {
            get;
            set;
        }
        #endregion
    }
}
