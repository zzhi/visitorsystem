﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Model
{
    public class VisitInfo : ModelBase
    {
        public int VisitorId { get; set; }
        public int EmployeeId { get; set; }
        public string UserName { get; set; }
        public string EmpName { get; set; }
        public string VisitorCardInfo { get; set; }
        public DateTime? RegisterTime { get; set; }
        public DateTime? CheckinTime { get; set; }
        public bool? Leave { get; set; }
        public DateTime? LeaveTime { get; set; }


        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { set; get; }

    }
}
