﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Model
{
    public class Employee : ModelBase
    {
        public string EmpName { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string Floor { get; set; }
        public string Descriptions { get; set; }

     

    }
}
