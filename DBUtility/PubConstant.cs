﻿using System;
using System.Configuration;
using System.Data;
namespace VMS.DBUtility
{
    public class PubConstant
    {

        public static string ConnectionString
        {
            get
            {
                string _connectionString = ConfigurationManager.AppSettings["ConnectionString"];

                return _connectionString;
            }
        }


    }
}
